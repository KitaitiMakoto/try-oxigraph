require "json/ld"
require "sparql/client"

ENDPOINT = "http://localhost:7878/update"

annotation = JSON.load(File.open("./annotations.jsonld"))
data = RDF::Graph.new
# JSONのデータを抽象的なRDFモデルに変換
data << JSON::LD::API.toRdf(annotation)
sparql = SPARQL::Client.new(ENDPOINT)
# クエリーにはSPARQL言語とTurtleなどのデータ表現が使われるけど、
# ライブラリーがRDFモデルからいい感じに組み立ててくれる
sparql.insert_data(data)
